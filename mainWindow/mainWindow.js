

var app = require('electron').remote;
var dialog = app.dialog;
var fs = require('fs');
const remote = require('electron').remote
const main = remote.require('./main.js')
var Cryptox = require("cryptox");
var exchange = new Cryptox("bitfinex");
var usd = new Cryptox("oxr", {key: "276483379a144176b4c4f89172dbc0dd"});


document.getElementById("totalProfit").style.visibility = "hidden";
document.getElementById("profitOne").style.visibility = "hidden";
document.getElementById("profitTwo").style.visibility = "hidden";

document.getElementById("cryptoOne").style.visibility = "hidden";
document.getElementById("cryptoOneValue").style.visibility = "hidden";
document.getElementById("cryptoTwo").style.visibility = "hidden";
document.getElementById("cryptoTwoValue").style.visibility = "hidden";






var setPreis = document.getElementById('calculate')
setPreis.addEventListener('click', () => {



  var inputValues = {
    "totalPaid": parseFloat(document.getElementById('totalPaid').value),
    "totalFees": parseFloat(document.getElementById('totalFees').value),
    "investmentOne": parseFloat(document.getElementById('investmentOne').value),
    "investmentTwo": parseFloat(document.getElementById('investmentTwo').value),
    "totalTokens": parseFloat(document.getElementById('totalTokens').value),
    "tokenExchange": parseFloat(document.getElementById('tokenExchange').value),
    "fiatExchange": parseFloat(document.getElementById('fiatExchange').value),
    "fiatExchange": parseFloat(document.getElementById('fiatExchange').value),
  
  }

  var invested = inputValues.totalPaid - inputValues.totalFees;
  
  document.getElementById('investmentNoFee').innerHTML = Number((invested).toFixed(3)) + ' €';
  
  //Calculate Percentage of Two
  var percentageOne = inputValues.investmentOne * 100 / invested;
  var percentageTwo = inputValues.investmentTwo * 100 / invested;

  document.getElementById("cryptoOne").style.visibility = "visible";
  document.getElementById("cryptoOneValue").style.visibility = "visible";
  document.getElementById("loading-total").style.visibility = "hidden";

  document.getElementById('cryptoOne').innerHTML = Number((percentageOne).toFixed(6)) + ' %';
  document.getElementById('cryptoTwo').innerHTML = Number((percentageTwo).toFixed(6)) + ' %';

  

//Calculate Tokens
  var cryptoOneValue = percentageOne * inputValues.totalTokens / 100;  
  var cryptoTwoValue = percentageTwo * inputValues.totalTokens / 100;  
  document.getElementById("cryptoTwo").style.visibility = "visible";
  document.getElementById("cryptoTwoValue").style.visibility = "visible";
  document.getElementById("loading-one").style.visibility = "hidden";
  
  document.getElementById("loading-two").style.visibility = "hidden";


  document.getElementById('cryptoOneValue').innerHTML = Number((cryptoOneValue).toFixed(6)) + ' MIOTA';
  document.getElementById('cryptoTwoValue').innerHTML = Number((cryptoTwoValue).toFixed(6)) + ' MIOTA';
  


//Calculate Exchange Bought
var boughtExchange = invested / inputValues.totalTokens;
document.getElementById('fiatExchange').innerHTML = Number((boughtExchange).toFixed(6)) + ' €';


//Currency Conversion

var currentIOTUSDRate, currentUSDEURRate

exchange.getRate({pair: "IOT_USD"}, function (err, rate) {
  if (!err)
 
  currentIOTUSDRate = parseFloat(rate.data["0"].rate)
  console.log(currentIOTUSDRate)
  return currentIOTUSDRate


  });

  usd.getRate({pair: "EUR_USD"}, function (err, rate) {
    if (!err)
        currentUSDEURRate = parseFloat(rate.data["0"].rate);
        console.log(currentUSDEURRate)
        return currentUSDEURRate;
    });


  var totalProfit
  var profitOne
  var profitTwo

  setTimeout(function calculateProfit(){
    totalProfit = (inputValues.totalTokens * currentIOTUSDRate * currentUSDEURRate) - inputValues.totalPaid;

    profitOne = ((percentageOne * totalProfit) / 100) - inputValues.investmentOne;
    profitTwo = ((percentageTwo * totalProfit) / 100) - inputValues.investmentTwo;

    document.getElementById('profitOne').innerHTML = Number((parseFloat(profitOne)).toFixed(3)) + ' €';
    document.getElementById('profitTwo').innerHTML = Number((parseFloat(profitTwo)).toFixed(3)) + ' €';
    document.getElementById("loading-profit-one").style.visibility = "hidden";
    document.getElementById("loading-profit-two").style.visibility = "hidden";
    

    document.getElementById('totalProfit').innerHTML = Number((parseFloat(totalProfit)).toFixed(3)) + ' €';
    document.getElementById("loading-total").style.visibility = "hidden";
    
    document.getElementById("totalProfit").style.visibility = "visible";
    document.getElementById("profitOne").style.visibility = "visible";
    document.getElementById("profitTwo").style.visibility = "visible";
    

    console.log(parseFloat(totalProfit))
  
  },
    
    4000); 
    
  

}, false)






